package com.tmaproject.androidci.data.comment;

import com.tmaproject.androidci.data.post.Post;
import io.reactivex.Single;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CommentService {


  @GET("posts/{postId}/comments")
  Single<List<Comment>> get(@Path("postId") String postId);

  @POST("posts/{postId}/comments")
  Single<ResponseBody> get(@Path("postId") String postId,@Body Comment comment);

  @DELETE("posts/{postId}/comments/{commentId}")
  Single<ResponseBody> delete(@Path("postId") String postId,@Path("commentId") String commentId);
}
