package com.tmaproject.androidci.data.auth;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Action;

public class AuthLocalStore implements AuthStore {

  private AuthTokens tokens = null;


  @Override
  public Completable saveToken(AuthTokens tokens) {
    return Completable.fromAction(()-> this.tokens = tokens);
  }

  @Override
  public Single<AuthTokens> getTokens() {
    if(tokens == null)
      return Single.error(new NotLoggedIn());

    return Single.fromCallable(()-> tokens);
  }

  @Override
  public Completable logout() {
    return Completable.fromAction(() -> tokens = null);
  }

}
