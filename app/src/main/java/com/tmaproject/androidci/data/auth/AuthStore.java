package com.tmaproject.androidci.data.auth;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AuthStore {

  Completable saveToken(AuthTokens tokens);

  Single<AuthTokens> getTokens();

  Completable logout();

}
