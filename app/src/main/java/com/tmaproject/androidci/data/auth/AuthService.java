package com.tmaproject.androidci.data.auth;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;

public interface AuthService {

  @GET("/auth/register")
  Single<AuthTokens> register(@Body RegistrationRequest request);

  @GET("/auth/login")
  Single<AuthTokens> login(@Body LoginRequest request);

}
