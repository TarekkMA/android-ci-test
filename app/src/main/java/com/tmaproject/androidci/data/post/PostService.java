package com.tmaproject.androidci.data.post;

import io.reactivex.Single;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PostService {

  @GET("posts")
  Single<List<Post>> list();

  @GET("posts/{postId}")
  Single<Post> get(@Path("postId") String postId);

  @POST("posts")
  Single<ResponseBody> post(@Body Post post);

  @DELETE("posts/{postId}")
  Single<ResponseBody> delete();

}
