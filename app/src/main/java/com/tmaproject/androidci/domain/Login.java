package com.tmaproject.androidci.domain;

import com.tmaproject.androidci.data.auth.AuthLocalStore;
import com.tmaproject.androidci.data.auth.AuthService;
import com.tmaproject.androidci.data.auth.AuthStore;
import com.tmaproject.androidci.data.auth.AuthTokens;
import com.tmaproject.androidci.data.auth.LoginRequest;
import com.tmaproject.androidci.domain.Login.Params;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;

public class Login extends SingleUseCase<Login.Params, AuthTokens> {


  private final AuthService authService;
  private final AuthLocalStore authLocalStore;

  public Login(AuthService authService,
      AuthLocalStore authLocalStore,
      Scheduler subscriptionScheduler,
      Scheduler postExcutionScheduler) {
    super(subscriptionScheduler, postExcutionScheduler);
    this.authService = authService;
    this.authLocalStore = authLocalStore;
  }

  @Override
  protected Single<AuthTokens> build(Params params) {
    LoginRequest request = new LoginRequest();
    request.setPassword(params.password);
    request.setUsername(params.username);
    return authService.login(request)
        .doOnSuccess(tokens -> {
          authLocalStore.saveToken(tokens).blockingAwait();
        });
  }

  public static final class Params {

    private final String username;
    private final String password;

    public Params(String username, String password) {
      this.username = username;
      this.password = password;
    }
  }
}
