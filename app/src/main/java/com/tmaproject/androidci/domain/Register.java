package com.tmaproject.androidci.domain;

import com.tmaproject.androidci.data.auth.AuthLocalStore;
import com.tmaproject.androidci.data.auth.AuthService;
import com.tmaproject.androidci.data.auth.AuthTokens;
import com.tmaproject.androidci.data.auth.RegistrationRequest;
import io.reactivex.Scheduler;
import io.reactivex.Single;

public class Register extends SingleUseCase<Register.Params, AuthTokens> {


  private final AuthService authService;
  private final AuthLocalStore authLocalStore;

  public Register(AuthService authService,
      AuthLocalStore authLocalStore,
      Scheduler subscriptionScheduler,
      Scheduler postExcutionScheduler) {
    super(subscriptionScheduler, postExcutionScheduler);
    this.authService = authService;
    this.authLocalStore = authLocalStore;
  }

  @Override
  protected Single<AuthTokens> build(Params params) {
    RegistrationRequest request = new RegistrationRequest();
    request.setPassword(params.password);
    request.setEmail(params.email);
    request.setUsername(params.username);
    return authService.register(request)
        .doOnSuccess(tokens -> {
          authLocalStore.saveToken(tokens).blockingAwait();
        });
  }

  public static final class Params {

    private final String username;
    private final String password;
    private final String email;

    public Params(String username, String password, String email) {
      this.username = username;
      this.password = password;
      this.email = email;
    }
  }
}
