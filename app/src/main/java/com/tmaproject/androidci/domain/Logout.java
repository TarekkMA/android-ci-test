package com.tmaproject.androidci.domain;

import com.tmaproject.androidci.data.auth.AuthLocalStore;
import com.tmaproject.androidci.data.auth.AuthService;
import com.tmaproject.androidci.data.auth.AuthTokens;
import com.tmaproject.androidci.data.auth.RegistrationRequest;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;

public class Logout extends CompletableUseCase<Void> {


  private final AuthLocalStore authLocalStore;

  public Logout(AuthLocalStore authLocalStore,
      Scheduler subscriptionScheduler,
      Scheduler postExcutionScheduler) {
    super(subscriptionScheduler, postExcutionScheduler);
    this.authLocalStore = authLocalStore;
  }

  @Override
  protected Completable build(Void v) {
    return authLocalStore.logout();
  }
}
