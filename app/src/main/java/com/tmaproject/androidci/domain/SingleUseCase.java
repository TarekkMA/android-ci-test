package com.tmaproject.androidci.domain;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public abstract class SingleUseCase<IN, OUT> {

  private Scheduler subscriptionScheduler;
  private Scheduler postExecutionScheduler;

  CompositeDisposable disposables = new CompositeDisposable();

  public SingleUseCase(Scheduler subscriptionScheduler, Scheduler postExcutionScheduler) {
    this.subscriptionScheduler = subscriptionScheduler;
    this.postExecutionScheduler = postExcutionScheduler;
  }

  protected abstract Single<OUT> build(IN in);

  public void excute(DisposableSingleObserver<OUT> observer, IN in) {
    disposables.add(
        build(in)
            .subscribeOn(subscriptionScheduler)
            .observeOn(postExecutionScheduler)
            .subscribeWith(observer)
    );
  }


  public void dispose(){
    if(!disposables.isDisposed()){
      disposables.dispose();
    }
  }


}
