package com.tmaproject.androidci.domain;

import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;

public abstract class CompletableUseCase<IN> {

  private Scheduler subscriptionScheduler;
  private Scheduler postExecutionScheduler;

  CompositeDisposable disposables = new CompositeDisposable();

  public CompletableUseCase(Scheduler subscriptionScheduler, Scheduler postExcutionScheduler) {
    this.subscriptionScheduler = subscriptionScheduler;
    this.postExecutionScheduler = postExcutionScheduler;
  }

  protected abstract Completable build(IN in);

  public void excute(DisposableCompletableObserver observer, IN in) {
    disposables.add(
        build(in)
            .subscribeOn(subscriptionScheduler)
            .observeOn(postExecutionScheduler)
            .subscribeWith(observer)
    );
  }


  public void dispose(){
    if(!disposables.isDisposed()){
      disposables.dispose();
    }
  }


}
