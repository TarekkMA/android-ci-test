package com.tmaproject.androidci.di;

import com.fasterxml.jackson.databind.ObjectMapper;
import dagger.Module;
import dagger.Provides;
import java.util.TimeZone;


@Module
public class CommonModule {

  @Provides
  public ObjectMapper objectMapper(){
    return new ObjectMapper().setTimeZone(TimeZone.getDefault());
  }

}
