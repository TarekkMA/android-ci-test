package com.tmaproject.androidci.di;


import com.tmaproject.androidci.data.auth.AuthService;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ServicesModule {


  @Provides
  public AuthService authService(Retrofit retrofit){
    return retrofit.create(AuthService.class);
  }

}
