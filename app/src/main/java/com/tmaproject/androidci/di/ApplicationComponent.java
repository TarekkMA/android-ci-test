package com.tmaproject.androidci.di;


import dagger.Component;

@ApplicationScope
@Component(
    modules = {
        AndroidModule.class,
        ServicesModule.class,
        CommonModule.class
    }
)
public interface ApplicationComponent {

}
