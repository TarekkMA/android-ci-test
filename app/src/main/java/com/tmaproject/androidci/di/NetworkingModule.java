package com.tmaproject.androidci.di;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;

import android.app.Application;
import com.tmaproject.androidci.BuildConfig;
import dagger.Module;
import dagger.Provides;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;


@Module
public class NetworkingModule {


  @Provides
  @ApplicationScope
  public OkHttpClient okHttpClient(Application application) {

    long cacheSize = 10 * 1024 * 1024L;
    Cache cache = new Cache(application.getCacheDir(), cacheSize);

    return new OkHttpClient.Builder()
        .addInterceptor(new HttpLoggingInterceptor().setLevel((BuildConfig.DEBUG) ? BODY : NONE))
        .connectTimeout(15, TimeUnit.SECONDS)
        .cache(cache)
        .build();
  }


  @Provides
  @ApplicationScope
  public Retrofit retrofit(OkHttpClient httpClient){
    return new Retrofit.Builder()
        .addConverterFactory(JacksonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(httpClient)
        .baseUrl("http://localhost:3000/api/")
        .build();
  }

}
