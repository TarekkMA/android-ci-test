package com.tmaproject.androidci.data.comment;

import static org.junit.Assert.*;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmaproject.androidci.data.post.Post;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import org.junit.Test;

public class CommentTest {



  @Test
  public void test_parsing() throws Exception {

    String json = "{\n"
        + "        \"_id\": \"5baea5c2cd9cb04130100a26\",\n"
        + "        \"content\": \"Hello\",\n"
        + "        \"userRef\": \"5baea4d3428b6b408799599c\",\n"
        + "        \"createdAt\": \"2018-09-28T22:05:54.338Z\",\n"
        + "        \"updatedAt\": \"2018-09-28T22:05:54.338Z\"\n"
        + "    }";

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.setTimeZone(TimeZone.getDefault());

    Post parsed = objectMapper.readValue(json, Post.class);

    Calendar c = Calendar.getInstance();
    c.set(Calendar.YEAR, 2018);
    c.set(Calendar.MONTH, 9 - 1);
    c.set(Calendar.DAY_OF_MONTH, 28);
    c.set(Calendar.HOUR_OF_DAY, 22);
    c.set(Calendar.MINUTE, 5);
    c.set(Calendar.SECOND, 54);
    c.set(Calendar.MILLISECOND, 338);

    assertEquals("5baea5c2cd9cb04130100a26", parsed.getId());
    assertEquals("Hello", parsed.getContent());
    assertEquals("5baea4d3428b6b408799599c", parsed.getUserRef());
    assertEquals(c.getTime(), parsed.getCreatedAt());
    assertEquals(c.getTime(), parsed.getUpdatedAt());

  }


}