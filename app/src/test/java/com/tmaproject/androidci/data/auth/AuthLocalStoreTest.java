package com.tmaproject.androidci.data.auth;

import static org.junit.Assert.*;

import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;

public class AuthLocalStoreTest {


  AuthLocalStore store;
  AuthTokens fakeTokens;

  @Before
  public void setup() {
    store = new AuthLocalStore();
    fakeTokens = new AuthTokens();
    fakeTokens.setId("1231231");
    fakeTokens.setUserRef("1231afsd");
    fakeTokens.setToken("vlmfdpfsdf");
    Calendar c = Calendar.getInstance();
    fakeTokens.setUpdatedAt(c.getTime());
    c.add(Calendar.YEAR, 100);
    fakeTokens.setCreatedAt(c.getTime());
  }


  @Test
  public void saveToken_withNoErrors() {
    TestObserver<AuthTokens> s = new TestObserver<>();


    TestObserver saveObserver = new TestObserver();
    store.saveToken(fakeTokens).subscribe(saveObserver);

    saveObserver.assertComplete();

    store.getTokens().subscribe(s);

    s.assertNoErrors()
        .assertValue(fakeTokens);
  }


  @Test
  public void getTokens_tokenNotSaved() {
    TestObserver<AuthTokens> s = new TestObserver<>();
    store.getTokens().subscribe(s);
    s.assertError(NotLoggedIn.class);
  }


  @Test
  public void getTokens_tokenSaved() {
    TestObserver<AuthTokens> s = new TestObserver<>();

    store.saveToken(fakeTokens).subscribe(new TestObserver<>());

    store.getTokens().subscribe(s);
    s.assertValue(fakeTokens);
  }


  @Test
  public void logout() {
    TestObserver<AuthTokens> tokensSavedObserver = new TestObserver<>();
    store.saveToken(fakeTokens).subscribe(new TestObserver<>());
    store.getTokens().subscribe(tokensSavedObserver);
    tokensSavedObserver.assertValue(fakeTokens);


    TestObserver logoutObserver = new TestObserver();
    store.logout().subscribe(logoutObserver);
    logoutObserver.assertComplete();

    TestObserver<AuthTokens> tokensNotThereObserver = new TestObserver<>();

    store.getTokens().subscribe(tokensNotThereObserver);

    tokensNotThereObserver.assertError(NotLoggedIn.class);
  }
}