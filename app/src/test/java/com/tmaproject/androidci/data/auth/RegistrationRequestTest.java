package com.tmaproject.androidci.data.auth;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmaproject.androidci.di.AndroidModule;
import com.tmaproject.androidci.di.ApplicationComponent;
import com.tmaproject.androidci.di.DaggerApplicationComponent;
import org.junit.Test;

public class RegistrationRequestTest {

  @Test
  public void test_parsing() throws Exception {

    String json = "{\n"
        + "\t\"username\":\"admin1\",\n"
        + "\t\"password\":\"123\",\n"
        + "\t\"email\":\"admin1@gmail.com\"\n"
        + "}";

    ObjectMapper objectMapper = new ObjectMapper();
    RegistrationRequest parsed = objectMapper.readValue(json,RegistrationRequest.class);

    assertEquals("admin1",parsed.getUsername());
    assertEquals("123",parsed.getPassword());
    assertEquals("admin1@gmail.com",parsed.getEmail());

  }


}