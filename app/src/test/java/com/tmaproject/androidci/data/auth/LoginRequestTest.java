package com.tmaproject.androidci.data.auth;

import static org.junit.Assert.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Calendar;
import java.util.TimeZone;
import org.junit.Test;

public class LoginRequestTest {

  @Test
  public void test_parsing() throws Exception {

    String json = "{\n"
        + "\t\"username\":\"admin1\",\n"
        + "\t\"password\":\"123\"\n"
        + "}";

    ObjectMapper objectMapper = new ObjectMapper();
    LoginRequest parsed = objectMapper.readValue(json,LoginRequest.class);

    assertEquals("admin1",parsed.getUsername());
    assertEquals("123",parsed.getPassword());

  }


}