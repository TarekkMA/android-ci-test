package com.tmaproject.androidci.data.auth;

import static org.junit.Assert.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Calendar;
import java.util.TimeZone;
import org.junit.Test;

public class AuthTokensTest {


  @Test
  public void test_parsing() throws Exception {
    String json = "{\n"
        + "    \"_id\": \"5baea4d3428b6b408799599d\",\n"
        + "    \"token\": \"gggQjO6TIz2199HA1ZOEMid1Cyy3xO77qJAYIEgaJS3uHt3WWU\",\n"
        + "    \"userRef\": \"5baea4d3428b6b408799599c\",\n"
        + "    \"createdAt\": \"2018-09-28T22:01:55.258Z\",\n"
        + "    \"updatedAt\": \"2018-09-28T22:01:55.258Z\"\n"
        + "}";


    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.setTimeZone(TimeZone.getDefault());
    AuthTokens parsed = objectMapper.readValue(json,AuthTokens.class);

    Calendar c = Calendar.getInstance();
    c.set(Calendar.YEAR,2018);
    c.set(Calendar.MONTH,9-1);
    c.set(Calendar.DAY_OF_MONTH,28);
    c.set(Calendar.HOUR_OF_DAY,22);
    c.set(Calendar.MINUTE,1);
    c.set(Calendar.SECOND,55);
    c.set(Calendar.MILLISECOND,258);

    assertEquals(c.getTime(),parsed.getCreatedAt());
    assertEquals(c.getTime(),parsed.getCreatedAt());
    assertEquals("5baea4d3428b6b408799599c",parsed.getUserRef());
    assertEquals("gggQjO6TIz2199HA1ZOEMid1Cyy3xO77qJAYIEgaJS3uHt3WWU",parsed.getToken());
    assertEquals("5baea4d3428b6b408799599d",parsed.getId());
  }

}